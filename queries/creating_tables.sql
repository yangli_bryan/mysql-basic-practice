CREATE DATABASE student_management_sys_yl DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

USE student_management_sys_yl;

CREATE TABLE student (
	`id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(500) NOT NULL,
    `age` INT NOT NULL,
    `sex` CHAR NOT NULL,
    `phone` CHAR(11) NOT NULL,
    PRIMARY KEY (id),
    CHECK (age > 0),
    CHECK (sex = '男' OR sex = '女'),
    CHECK (length(phone) = 11)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
    
CREATE TABLE teacher (
	`id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(500) NOT NULL,
    `age` INT NOT NULL,
    `sex` CHAR NOT NULL,
    `subject_id` INT NOT NULL,
    PRIMARY KEY (id),
    CHECK (age > 0),
    CHECK (sex = '男' OR sex = '女')
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
    
CREATE TABLE subject (
	`id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(500) NOT NULL,
    `period` INT NOT NULL,
    `description` TEXT,
    PRIMARY KEY (id)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
    
CREATE TABLE exam (
	`id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(500) NOT NULL,
    `subject_id` INT NOT NULL,
    PRIMARY KEY (id)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE score (
	`id` INT NOT NULL AUTO_INCREMENT,
    `student_id` INT NOT NULL,
    `exam_id` INT NOT NULL,
    `score` FLOAT NOT NULL,
    PRIMARY KEY (id)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE teacher
ADD `phone` CHAR(11) NOT NULL CHECK (length(phone) = 11);

ALTER TABLE score
ADD CHECK(score >= 0 AND score <= 100);

ALTER TABLE student
RENAME COLUMN `name` TO `gender`;

ALTER TABLE teacher
RENAME COLUMN `name` TO `gender`;

DROP TABLE score;

CREATE TABLE score (
	`id` INT NOT NULL AUTO_INCREMENT,
    `exam_id` INT NOT NULL,
    `student_id` INT NOT NULL,
    `score` FLOAT NOT NULL,
    PRIMARY KEY (id),
    CHECK (score >= 0 AND score <= 100)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
