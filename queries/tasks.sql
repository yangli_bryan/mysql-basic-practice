USE student_management_sys_yl;

SELECT * FROM score
WHERE exam_id = (SELECT id FROM exam
				 WHERE name = '期末考试' AND
					   subject_id = (SELECT id FROM subject
									 WHERE name = '语文'));
								
SELECT * FROM student
WHERE id IN (SELECT student_id FROM score
			WHERE exam_id = (SELECT id FROM exam
							 WHERE name = '中期考试' AND 
                             subject_id = (SELECT id FROM subject WHERE name = '语文')));
                             
SELECT * FROM student
WHERE id IN (SELECT student_id FROM score
			 WHERE exam_id = (SELECT id FROM exam
							  WHERE name = '中期考试' AND 
                              subject_id = (SELECT id FROM subject WHERE name = '语文'))
				   AND score > 60);

SELECT * FROM student
WHERE id IN (SELECT student_id FROM score
			 WHERE exam_id = (SELECT id from exam
							  WHERE name = '期末考试' AND
									subject_id = (SELECT subject_id FROM teacher
												  WHERE name = '李老师'))
				   AND score >= 60);