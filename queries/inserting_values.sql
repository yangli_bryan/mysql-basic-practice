USE student_management_sys_yl;
ALTER TABLE student
RENAME COLUMN `gender` to `name`;

ALTER TABLE teacher
RENAME COLUMN `gender` to `name`;

INSERT INTO student VALUES (1, '张三', 18, '男', '15342349123');
INSERT INTO student VALUES (2, '李四', 20, '女', '13884356789');
INSERT INTO student VALUES (3, '王五', 26, '女', '13884359789');
INSERT INTO student VALUES (4, '赵六', 16, '女', '13884334789');

INSERT INTO teacher VALUES (1, '刘老师', 38, '男', 1, '15343349123');
INSERT INTO teacher VALUES (2, '李老师', 40, '女', 2, '13885356789');
INSERT INTO teacher VALUES (3, '张老师', 56, '女', 3, '18784359789');

INSERT INTO subject VALUES (1, '语文', 40, ' ');
INSERT INTO subject VALUES (2, '数学', 30, ' ');
INSERT INTO subject VALUES (3, '英语', 45, ' ');

INSERT INTO exam VALUES (1, '期末考试', 1);
INSERT INTO exam VALUES (2, '期末考试', 2);
INSERT INTO exam VALUES (3, '中期考试', 3);
INSERT INTO exam VALUES (4, '中期考试', 1);

INSERT INTO score VALUES (1, 1, 1, 80);
INSERT INTO score VALUES (2, 1, 2, 60);
INSERT INTO score VALUES (3, 1, 3, 70);
INSERT INTO score VALUES (4, 1, 4, 60.5);
INSERT INTO score VALUES (5, 2, 1, 40);
INSERT INTO score VALUES (6, 2, 2, 30);
INSERT INTO score VALUES (7, 2, 3, 20);
INSERT INTO score VALUES (8, 2, 4, 55);
INSERT INTO score VALUES (9, 3, 1, 48);
INSERT INTO score VALUES (10, 3, 2, 39);
INSERT INTO score VALUES (11, 3, 3, 70);
INSERT INTO score VALUES (12, 3, 4, 95);
INSERT INTO score VALUES (13, 4, 1, 78);
INSERT INTO score VALUES (14, 4, 2, 88);
INSERT INTO score VALUES (15, 4, 3, 0);
INSERT INTO score VALUES (16, 4, 4, 100);

UPDATE student
SET phone = '13456789876'
WHERE name = '赵六';

UPDATE subject
SET description = '该课程是本校特殊课程，由非常资深的刘老师授课。'
WHERE name = '语文';

UPDATE score
SET score = 98
WHERE student_id = (SELECT id FROM student WHERE name = '赵六') AND
	  exam_id = (SELECT id FROM exam WHERE name = '中期考试' AND 
				 subject_id = (SELECT id FROM subject WHERE name = '语文'));
                 
SELECT * FROM student;

SELECT * FROM student
WHERE age >= 18;

DELETE FROM score
WHERE student_id = (SELECT id FROM student WHERE name = '赵六');